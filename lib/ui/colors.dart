import 'dart:ui';

class Colors {
  static const blue = const Color(0xFF0014a8);
  static const blue2 = const Color(0xFF1447fc);
  static const red = const Color(0xffff4c4c);
  static const orange = const Color(0xFFFF675D);
  static const black = const Color(0xFF000000);
  static const white = const Color(0xFFFFFFFF);
  static const ffaeaeb2 = const Color(0xFFAEAEB2);
  static const ffc4c4c4 = const Color(0xFFC4C4C4);
  static const gray3 = const Color(0xFFEEEEEE);
  static const ff454545 = const Color(0xFF454545);
  static const gray5 = const Color(0xFFF5F5F5);
  static const ff8e8e93 = const Color(0xFF8E8E93);
  static const gray7 = const Color(0xFFD6D6D6);
  static const pink = const Color(0xFFFF675D);
  static const pinkAppBar = const Color(0xFFFCC5C1);
  static const gray8 = const Color(0xFFFFFEFE);
  static const gray15 = const Color(0xFFF9F9F9);
  static const gray9 = const Color(0xFF424242);
  static const gray10 = const Color(0xFF515152);
  static const ff686868 = const Color(0xFF686868);
  static const gray12 = const Color(0xFFF1F1F1);
  static const gray13 = const Color(0xFFF9F9F9);
  static const fff0f0f0 = const Color(0xFFF0F0F0);
  static const ff3c3c43 = const Color(0xFF3C3C43);
  static const ffa0a0a0 = const Color(0xffa0a0a0);
  static const fff7f7fa = const Color(0xfff7f7fa);
  static const transparent = const Color(0x00);
  static const ff696969 = const Color(0xFF696969);
  static const ff424242 = const Color(0xFF424242);
}
