import 'dart:ui';


import 'input/input.dart' as ui;
import 'loading_widget.dart' as ui;

class Widgets {
  static ui.Input input = const ui.Input();

  static ui.LoadingWidget loadingWidget({required double size, required Color androidColor}) =>
      ui.LoadingWidget(size: size, androidColor: androidColor);
}
