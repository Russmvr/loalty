import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../colors.dart' as ui;
import '../../../styles.dart' as ui;

class UnderlineInputField extends StatelessWidget {
  UnderlineInputField(
      {String? text,
      required String hintText,
      Function(String text)? onChanged,
      String? errorMessage,})
      : this._text = text ?? '',
        _hintText = hintText,
        _onChanged = onChanged,
        _errorMessage = errorMessage ?? null;

  final String _text;
  final String _hintText;
  final Function(String test)? _onChanged;
  final String? _errorMessage;

  @override
  Widget build(BuildContext context) {
    return _containerTextField;
  }

  Widget get _containerTextField => Container(
        height: 60,
        width: double.infinity,
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Stack(
          children: <Widget>[
            Positioned(
              left: 0,
              right: 0,
              bottom: 1,
              child: _textField,
            ),
          ],
        ),
      );

  Widget get _textField => Material(
        child: TextField(
          keyboardType: TextInputType.text,
          style: ui.Styles.montserrat_w400_s18_black_sizer,
          cursorColor: ui.Colors.orange,
          onChanged: (text) => (_onChanged != null) ? _onChanged!(text) : null,
          decoration: InputDecoration(
            helperText: ' ',
            filled: true,
            fillColor: ui.Colors.transparent,
            contentPadding: EdgeInsets.symmetric(vertical: 10),
            hintText: _hintText,
            hintStyle: ui.Styles.montserrat_w400_s18_black_sizer,
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                width: 1.5,
                color: 
                    ui.Colors.black.withOpacity(0.3)
              ),
            ),
            errorText: _errorMessage,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                width: 1.5,
                color: ui.Colors.blue,
              ),
            ),
          ),
        ),
      );
}
