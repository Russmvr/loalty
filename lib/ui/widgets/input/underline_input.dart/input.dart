import 'input_field.dart' as ui;

class Input {
  const Input();
  ui.UnderlineInputField textField({
    String? text,
    required String hintText,
    required Function(String text) onChanged,
    String? errorMessage,
  }) =>
      ui.UnderlineInputField(
        onChanged: onChanged,
        errorMessage: errorMessage,
        text: text!,
        hintText: hintText,

      );
}
