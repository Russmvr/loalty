import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../colors.dart' as ui;

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({required this.size, this.androidColor});

  final double size;
  final Color? androidColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      child: (Platform.isAndroid)
          ? CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(
                  androidColor ?? ui.Colors.white))
          : CupertinoActivityIndicator(),
    );
  }
}
