import 'dart:ui';

import 'package:flutter/material.dart';
import 'colors.dart' as ui;
import 'package:sizer/sizer.dart';

const montserrat = 'Montserrat';
const consolas = 'Consolas';
const sourceSans = 'SourceSans';

class Styles {
  static const montserrat_s60_orange = const TextStyle(
    fontFamily: consolas,
    height: 2,
    fontSize: 60,
    letterSpacing: 0.3,
    color: ui.Colors.orange,
  );

  static var montserrat_w400_s26_black_sizer = TextStyle(
    fontFamily: sourceSans,
    fontWeight: FontWeight.w600,
    fontSize: 26.sp,
    height: 1.3,
    letterSpacing: -0.5,
    color: ui.Colors.black,
  );

  static var montserrat_w400_s18_black_sizer = TextStyle(
    fontFamily: sourceSans,
    fontWeight: FontWeight.w500,
    fontSize: 18.sp,
    height: 1.3,
    letterSpacing: -0.5,
    color: ui.Colors.black,
  );

  static var montserrat_w400_s18_blue_sizer = TextStyle(
    fontFamily: sourceSans,
    fontWeight: FontWeight.w500,
    fontSize: 18.sp,
    height: 1.3,
    letterSpacing: -0.5,
    color: ui.Colors.blue2,
  );

  static var montserrat_w400_s18_white_sizer = TextStyle(
    fontFamily: consolas,
    fontWeight: FontWeight.w500,
    fontSize: 18.sp,
    height: 1.3,
    letterSpacing: -0.5,
    color: ui.Colors.white,
  );

  static var montserrat_w400_s22_blue_sizer = TextStyle(
    fontFamily: sourceSans,
    fontWeight: FontWeight.w500,
    fontSize: 22.sp,
    height: 1.3,
    letterSpacing: -0.5,
    color: ui.Colors.blue2,
  );

  static var montserrat_w400_s22_white_sizer = TextStyle(
    fontFamily: consolas,
    fontWeight: FontWeight.w500,
    fontSize: 22.sp,
    height: 1.3,
    letterSpacing: -0.5,
    color: ui.Colors.white,
  );
static const montserrat_w400_s18_orange = const TextStyle(
    fontFamily: sourceSans,
    fontWeight: FontWeight.w500,
    fontSize: 18,
    height: 1.3,
    letterSpacing: -0.5,
    color: ui.Colors.orange,
  );
  
}
