import 'package:flutter/material.dart';

class Images {
  static Image logo({Size? size}) => Image.asset(
        'assets/images/logo.png',
        height: size?.height,
        width: size?.width,
      );

}
