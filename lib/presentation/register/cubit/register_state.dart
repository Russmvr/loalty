abstract class RegisterState{}



class InitialState extends RegisterState{}


class NameFieldState extends RegisterState{
  NameFieldState({required this.name, this.error});

  final String name;
  final String? error;
}

class LoginFieldState extends RegisterState{
  LoginFieldState({required this.login, this.error});

  final String login;
  final String? error;
}

class PasswordFieldState extends RegisterState {
  PasswordFieldState({required this.password, this.error});

  final String password;
  final String? error;
}

class InvalidPassword extends RegisterState{}


class UserFound extends RegisterState{}
