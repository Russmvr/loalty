import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loalty/core/exception/auth_exception.dart';
import 'package:loalty/domain/models/user.dart';
import 'package:loalty/domain/usecase/register.dart';

import '../register.dart';

class RegisterCubit extends Cubit<RegisterState> {
  String _login;
  String _password;
  String _name;
  RegisterUsecase _registerUsecase;

  RegisterCubit({required RegisterUsecase registerUsecase})
      : _registerUsecase = registerUsecase,
        _login = '',
        _password = '',
        _name = '',
        super((InitialState()));

  void registerTap() {
    _registerUsecase.register(
        user: User(
            login: _login, password: _password, name: _name, isAuth: true));
  }


  void _handleRegisterException({required Exception exception}) {
    if (exception is UserFoundException) emit(UserFound());
  }

  void onNameChanged(String name) {
    _name = name;
    print('_name - $_name');
    emit(NameFieldState(name: _name));
  }

  void onLoginChanged(String login) {
    _login = login;
    print('_login - $_login');
    emit(LoginFieldState(login: _login));
  }

  void onPasswordChanged(String password) {
    _password = password;
    print('password - $_password');
    emit(PasswordFieldState(password: _password));
  }
}
