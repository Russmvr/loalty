import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loalty/domain/usecase/register.dart';
import '../register.dart';

class RegisterPage extends MaterialPage {
  RegisterPage()
      : super(
          child: BlocProvider(
            create: (context) => RegisterCubit(
                registerUsecase:
                    RepositoryProvider.of<RegisterUsecase>(context)),
            child: RegisterWidget(),
          ),
        );
}
