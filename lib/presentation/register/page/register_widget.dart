import 'package:flutter/material.dart' hide Colors;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loalty/ui/ui.dart';
import 'package:sizer/sizer.dart';

import '../register.dart';

class RegisterWidget extends StatelessWidget {
  const RegisterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: BlocBuilder<RegisterCubit, RegisterState>(
              buildWhen: (previous, current) {
            print('previous - $previous');
            print('current - $current');
            if (current is LoginFieldState ||
                current is PasswordFieldState ||
                current is NameFieldState) return false;
            return true;
          }, builder: (context, state) {
            if (state is InitialState) {
              return registerScreen(context);
            }
            return Container();
          })),
    );
  }

  Widget registerScreen(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top: 5.h),
        alignment: Alignment.center,
        child: Column(
          children: [
            Column(
              children: [
                Stack(children: [
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                                      child: Padding(
                      padding: EdgeInsets.only(top: 7.5.h, left: 2.w),
                      child: Icon(
                        Icons.arrow_back,
                        size: 40.0,
                      ),
                    ),
                  ),
                  Container(
                      alignment: Alignment.topCenter,
                      child: Images.logo(size: Size(40.w, 40.w))),
                ]),
                SizedBox(height: 5.h),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    'Регистрация',
                    style: Styles.montserrat_w400_s26_black_sizer,
                  ),
                ),
                SizedBox(height: 2.h),
              ],
            ),
            Column(
              children: [
                BlocBuilder<RegisterCubit, RegisterState>(
                  builder: (context, state) {
                    if (state is NameFieldState) {
                      return nameWidget(name: state.name, context: context);
                    }
                    return nameWidget(context: context);
                  },
                ),
                BlocBuilder<RegisterCubit, RegisterState>(
                  builder: (context, state) {
                    if (state is LoginFieldState) {
                      return loginWidget(name: state.login, context: context);
                    }
                    return loginWidget(context: context);
                  },
                ),
                BlocBuilder<RegisterCubit, RegisterState>(
                  builder: (context, state) {
                    if (state is PasswordFieldState) {
                      return passwordWidget(
                          name: state.password, context: context);
                    }
                    return passwordWidget(context: context);
                  },
                ),
                SizedBox(height: 3.h),
                registerButton(context),
              ],
            ),
          ],
        ));
  }
}

Widget nameWidget({
  required BuildContext context,
  String? name,
}) =>
    Container(
      padding: EdgeInsets.symmetric(horizontal: 12.w),
      height: 80,
      child: Widgets.input.underline.textField(
        onChanged: (text) =>
            BlocProvider.of<RegisterCubit>(context).onNameChanged(text),
        text: name ?? '',
        hintText: 'Имя',
      ),
    );

Widget loginWidget({
  required BuildContext context,
  String? name,
}) =>
    Container(
      padding: EdgeInsets.symmetric(horizontal: 12.w),
      height: 80,
      child: Widgets.input.underline.textField(
        onChanged: (text) =>
            BlocProvider.of<RegisterCubit>(context).onLoginChanged(text),
        text: name ?? '',
        hintText: 'Логин',
      ),
    );

Widget passwordWidget({
  required BuildContext context,
  String? name,
}) =>
    Container(
      padding: EdgeInsets.symmetric(horizontal: 12.w),
      height: 80,
      child: Widgets.input.underline.textField(
        onChanged: (text) =>
            BlocProvider.of<RegisterCubit>(context).onPasswordChanged(text),
        text: name ?? '',
        hintText: 'Пароль',
      ),
    );

Widget registerButton(BuildContext context) {
  return GestureDetector(
    onTap: () => BlocProvider.of<RegisterCubit>(context)
        .registerTap(),
    child: Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(36),
        color: Colors.blue2,
      ),
      width: 80.w,
      height: 8.h,
      child: Text('Зарегистрироваться', style: Styles.montserrat_w400_s22_white_sizer),
    ),
  );
}
