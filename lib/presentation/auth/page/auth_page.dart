import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loalty/domain/usecase/check_authorized.dart';
import 'package:loalty/domain/usecase/login.dart';
import '../auth.dart';

class AuthPage extends MaterialPage {
  AuthPage()
      : super(
          child: BlocProvider(
            create: (context) => AuthCubit(
                checkStatusUseCase:
                    RepositoryProvider.of<CheckAuthorizedUseCase>(context),
                loginUsecase: RepositoryProvider.of<LoginUsecase>(context)),
            child: AuthWidget(),
          ),
        );
}
