import 'package:flutter/material.dart' hide Colors;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loalty/presentation/register/page/page.dart';
import 'package:loalty/ui/ui.dart';
import 'package:sizer/sizer.dart';

import '../auth.dart';

class AuthWidget extends StatelessWidget {
  const AuthWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: BlocListener<AuthCubit, AuthState>(
            listener: (BuildContext context, AuthState state) {
              if (state is NavigateToRegisterPage) {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => RegisterPage().child,
                ));
              }
            },
            child: BlocBuilder<AuthCubit, AuthState>(
                buildWhen: (previous, current) {
              // print('previous - $previous');
              // print('current - $current');
              if (current is LoginFieldState ||
                  current is PasswordFieldState ||
                  current is NavigateToRegisterPage) return false;
              return true;
            }, builder: (context, state) {
              if (state is InitialState) {
                return loginScreen(context);
              }
              return Container();
            }),
          )),
    );
  }

  Widget loginScreen(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top: 5.h),
        alignment: Alignment.center,
        child: Column(
          children: [
            Column(
              children: [
                Container(
                    alignment: Alignment.topCenter,
                    child: Images.logo(size: Size(40.w, 40.w))),
                SizedBox(height: 5.h),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    'Войти',
                    style: Styles.montserrat_w400_s26_black_sizer,
                  ),
                ),
                SizedBox(height: 2.h),
              ],
            ),
            Column(
              children: [
                BlocBuilder<AuthCubit, AuthState>(
                  buildWhen: (previous, current) {
                    if (current is PasswordFieldState) return false;
                    return true;
                  },
                  builder: (context, state) {
                    if (state is LoginFieldState) {
                      return loginWidget(
                          name: state.login,
                          context: context,
                          error: state.error);
                    }
                    return loginWidget(context: context);
                  },
                ),
                BlocBuilder<AuthCubit, AuthState>(
                  buildWhen: (previous, current) {
                    if (current is LoginFieldState) return false;
                    return true;
                  },
                  builder: (context, state) {
                    if (state is PasswordFieldState) {
                      return passwordWidget(
                          name: state.password,
                          context: context,
                          error: state.error);
                    }
                    return passwordWidget(context: context);
                  },
                ),
                SizedBox(height: 3.h),
                loginButton(context),
              ],
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(bottom: 5.h),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    GestureDetector(
                        onTap:
                            BlocProvider.of<AuthCubit>(context).toRegisterTap,
                        child: Text('Зарегистрироваться',
                            style: Styles.montserrat_w400_s22_blue_sizer))
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}

Widget loginWidget({
  required BuildContext context,
  String? error,
  String? name,
}) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 12.w),
    height: 80,
    child: Widgets.input.underline.textField(
      errorMessage: error,
      onChanged: (text) =>
          BlocProvider.of<AuthCubit>(context).onLoginChanged(text),
      text: name ?? '',
      hintText: 'Логин',
    ),
  );
}

Widget passwordWidget({
  required BuildContext context,
  String? error,
  String? name,
}) =>
    Container(
      padding: EdgeInsets.symmetric(horizontal: 12.w),
      height: 80,
      child: Widgets.input.underline.textField(
        errorMessage: error,
        onChanged: (text) =>
            BlocProvider.of<AuthCubit>(context).onPasswordChanged(text),
        text: name ?? '',
        hintText: 'Пароль',
      ),
    );

Widget loginButton(BuildContext context) {
  return GestureDetector(
    onTap: () => BlocProvider.of<AuthCubit>(context).loginTap(),
    child: Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(36),
        color: Colors.blue2,
      ),
      width: 40.w,
      height: 8.h,
      child: Text('Войти', style: Styles.montserrat_w400_s22_white_sizer),
    ),
  );
}
