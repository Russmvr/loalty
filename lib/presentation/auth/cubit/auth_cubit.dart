import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loalty/core/exception/auth_exception.dart';
import 'package:loalty/domain/usecase/check_authorized.dart';
import 'package:loalty/domain/usecase/login.dart';
import 'package:loalty/presentation/register/page/page.dart';

import '../auth.dart';

class AuthCubit extends Cubit<AuthState> {
  String _login;
  String _password;
  CheckAuthorizedUseCase _checkStatusUsecase;
  LoginUsecase _loginUsecase;

  AuthCubit(
      {required CheckAuthorizedUseCase checkStatusUseCase,
      required LoginUsecase loginUsecase})
      : _checkStatusUsecase = checkStatusUseCase,
        _loginUsecase = loginUsecase,
        _login = '',
        _password = '',
        super((InitialState())) {
    // _initial();
  }

  void _initial() {
    final isAuth = _checkStatusUsecase.checkAuthorized();
    _handleAuthorized(isAuth: isAuth.value!);
  }

  void _handleAuthorized({required bool isAuth}) {
    if (isAuth)
      emit(LoggedState());
    else
      emit(InitialState());
  }

  void toRegisterTap() {
    emit(NavigateToRegisterPage());
  }

  void loginTap() {
    bool _isErrorValidate = validateFields();
    if (!_isErrorValidate) {
      final _trySignin = _loginUsecase.login(login: _login, password: _password);
      if (_trySignin.isException)
        _handleLoginException(exception: _trySignin.exception!);
    }
  }

  
  // void _handleLoginException({required Exception exception}) {
  //   if (exception is InvalidPasswordException) emit(InvalidPassword());
  //   if (exception is NoUserFoundException) emit(NoUserFound());
  // }

  void _handleLoginException({required Exception exception}) {
    if (exception is InvalidPasswordException) emit(PasswordFieldState(
          password: _password, error: 'Неверный пароль'));
    if (exception is NoUserFoundException) emit(LoginFieldState(
          login: _login, error: 'Пользователь не найден'));
  }

  void onLoginChanged(String login) {
    _login = login;
    emit(LoginFieldState(login: _login));
  }

  void onPasswordChanged(String password) {
    _password = password;
    emit(PasswordFieldState(password: _password));
  }

  bool validateFields() {
    bool result = false;
    if (_login.isEmpty) {
      result = true;
      emit(LoginFieldState(
          login: _login, error: 'Данное поле не должно быть пустым'));
    }
    if (_password.isEmpty) {
      result = true;
      emit(PasswordFieldState(
          password: _password, error: 'Данное поле не должно быть пустым'));
    }
    return result;
  }
}
