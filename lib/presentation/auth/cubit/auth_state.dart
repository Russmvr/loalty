import 'package:loalty/presentation/auth/auth.dart';

abstract class AuthState{}



class InitialState extends AuthState{}

class LoggedState extends AuthState{}

class NavigateToRegisterPage extends AuthState{}


class NameFieldState extends AuthState{
  NameFieldState({required this.name, this.error});

  final String name;
  final String? error;
}

class LoginFieldState extends AuthState{
  LoginFieldState({required this.login, this.error});

  final String login;
  final String? error;
}

class PasswordFieldState extends AuthState {
  PasswordFieldState({required this.password, this.error});

  final String password;
  final String? error;
}

class InvalidPassword extends AuthState{}


class NoUserFound extends AuthState{}
