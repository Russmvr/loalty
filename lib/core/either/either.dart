class Either<T> {
  const Either.value(T value)
      // ignore: prefer_initializing_formals
      : value = value,
        exception = null;

  const Either.exception(Exception exception)
      // ignore: prefer_initializing_formals
      : exception = exception,
        value = null;

  final T? value;
  final Exception? exception;

  static Either<T> tryCreate<T>(T Function() call) {
    try {
      return Either.value(call());
    } catch (e) {
      return Either.exception(e is Exception ? e : Exception());
    }
  }

  static Future<Either<T>> tryCreateFuture<T>(Future<T> Function() call) async {
    try {
      return Either.value(await call());
    } catch (e) {
      return Either.exception(e is Exception ? e : Exception());
    }
  }

  bool get isValue => value != null;

  bool get isException => exception != null;
}
