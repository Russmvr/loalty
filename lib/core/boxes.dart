import 'package:hive/hive.dart';
import 'package:loalty/domain/models/user.dart';

class Boxes {
  static Box<User> getTransactions() =>
      Hive.box<User>('transactions');
}