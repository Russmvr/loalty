import 'package:flutter/material.dart';
import 'package:loalty/presentation/auth/auth.dart';
import 'package:sizer/sizer.dart';


class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Sizer(
          builder: (context, orientation, deviceType) => MaterialApp(
            onGenerateRoute: (_) {
              return AuthPage().createRoute(context);
            },
          ),
    );
  }
}
