import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:loalty/domain/models/user.dart';
import 'package:loalty/domain/usecase/check_authorized.dart';
import 'package:loalty/domain/usecase/get_orders.dart';
import 'package:loalty/domain/usecase/get_qr.dart';
import 'package:loalty/domain/usecase/get_score.dart';
import 'package:loalty/domain/usecase/get_user.dart';
import 'package:loalty/domain/usecase/login.dart';
import 'package:loalty/domain/usecase/logout.dart';
import 'package:loalty/domain/usecase/register.dart';
import 'package:loalty/repository/auth_repository.dart';
import 'package:loalty/repository/order_repository.dart';
import 'package:loalty/repository/qr_repository.dart';
import 'package:loalty/repository/score_repository.dart';
import 'package:loalty/services/local_auth_service.dart';
import 'package:loalty/services/local_order_service.dart';
import 'package:loalty/services/local_qr_service.dart';
import 'package:loalty/services/local_score_service.dart';
import 'package:loalty/storage/local_auth_storage.dart';
import 'app.dart';

Future<Widget> createInjectionContainer() async {
  // Creating Hive's box
  final usersBox = await Hive.openBox<List<User>>('users');

  // Creating Storage
  final localAuthStorage = LocalAuthStorage(box: usersBox);

  // Creating local Services
  final localAuthService = LocalAuthService(localAuthStorage: localAuthStorage);
  final localOrderService = LocalOrderService();
  final localQrService = LocalQrService();
  final localScoreService = LocalScoreService();

  // Creating Repos
  final authRepository = AuthRepository(localAuthService: localAuthService);
  final orderRepository = OrderRepository(localOrderService: localOrderService);
  final qrRepository = QrRepository(localQrService: localQrService);
  final scoreRepository = ScoreRepository(localScoreService: localScoreService);

  // Creating usecases
  final loginUsecase = LoginUsecase(authRepository: authRepository);
  final logoutUsecase = LogoutUsecase(authRepository: authRepository);
  final registerUsecase = RegisterUsecase(authRepository: authRepository);
  final getUserUsecase = GetUserUsecase(authRepository: authRepository);
  final checkAuthorizedUsecase = CheckAuthorizedUseCase(authRepository: authRepository);
  final getOrdersUsecase = GetOrdersUsecase(orderRepository: orderRepository);
  final getQrUsecase = GetQrUsecase(qrRepository: qrRepository);
  final getScoreUsecase = GetScoreUsecase(scoreRepository: scoreRepository);

  return MultiRepositoryProvider(
    providers: [
      RepositoryProvider<LoginUsecase>(
        create: (_) => loginUsecase,
      ),
      RepositoryProvider<LogoutUsecase>(
        create: (_) => logoutUsecase,
      ),
      RepositoryProvider<RegisterUsecase>(
        create: (_) => registerUsecase,
      ),
      RepositoryProvider<GetUserUsecase>(
        create: (_) => getUserUsecase,
      ),
      RepositoryProvider<CheckAuthorizedUseCase>(create: (_) => checkAuthorizedUsecase),
      RepositoryProvider<GetOrdersUsecase>(
        create: (_) => getOrdersUsecase,
      ),
      RepositoryProvider<GetQrUsecase>(create: (_) => getQrUsecase),
      RepositoryProvider<GetScoreUsecase>(
        create: (_) => getScoreUsecase,
      ),
    ],
    child: const App(),
  );
}
