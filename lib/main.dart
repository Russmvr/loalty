import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:loalty/domain/models/user.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'app/app_bloc_observer.dart';
import 'app/injection_container.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocumentDirectory =
      await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocumentDirectory.path);
  Hive.registerAdapter(UserAdapter());
  
  Bloc.observer = AppBlocObserver();

  final injectionContainer = await createInjectionContainer();

  runApp(injectionContainer);
}
