import 'package:loalty/core/either/either.dart';
import 'package:loalty/domain/repository/auth_repository.dart';

class LoginUsecase{
    const LoginUsecase({required IAuthRepository authRepository}) :
     _authRepository = authRepository;


    final IAuthRepository _authRepository;



    Either<void> login({
      required String login,
      required String password}){
      return Either.tryCreate(() => _authRepository.login(login: login, password: password));
    }
}