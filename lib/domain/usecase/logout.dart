import 'package:loalty/core/either/either.dart';
import 'package:loalty/domain/repository/auth_repository.dart';

class LogoutUsecase{
    const LogoutUsecase({required IAuthRepository authRepository}) :
     _authRepository = authRepository;


    final IAuthRepository _authRepository;


    Either<void> getOrders({required int id}){
      return Either.tryCreate(() => _authRepository.logout(id: id));
    }
}