import 'package:loalty/core/either/either.dart';
import 'package:loalty/domain/repository/auth_repository.dart';

class CheckAuthorizedUseCase{
  const CheckAuthorizedUseCase({required IAuthRepository authRepository}) : _authRepository = authRepository;

  final IAuthRepository _authRepository;

  Either<bool> checkAuthorized(){
    return Either.tryCreate(() => _authRepository.checkAuthorized());
  }
}