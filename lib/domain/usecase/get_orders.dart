import 'package:loalty/core/either/either.dart';
import 'package:loalty/domain/models/order.dart';
import 'package:loalty/domain/repository/order_repository.dart';

class GetOrdersUsecase{
    const GetOrdersUsecase({required IOrderRepository orderRepository}) : _orderRepository = orderRepository;

    final IOrderRepository _orderRepository;

    Either<List<Order>> getOrders(){
      return Either.tryCreate(() => _orderRepository.getOrders());
    }
}