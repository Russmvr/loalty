import 'package:loalty/core/either/either.dart';
import 'package:loalty/domain/models/user.dart';
import 'package:loalty/domain/repository/auth_repository.dart';

class RegisterUsecase{
    const RegisterUsecase({required IAuthRepository authRepository}) :
     _authRepository = authRepository;


    final IAuthRepository _authRepository;

    Either<void> register({required User user}){
      return Either.tryCreate(() => _authRepository.register(user: user));
    }
}