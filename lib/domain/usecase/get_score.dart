import 'package:loalty/core/either/either.dart';
import 'package:loalty/domain/repository/score_repository.dart';

class GetScoreUsecase{
    const GetScoreUsecase({required IScoreRepository scoreRepository}) : _scoreRepository = scoreRepository;

    final IScoreRepository _scoreRepository;

    Either<int> getOrders(){
      return Either.tryCreate(() => _scoreRepository.getScore());
    }
}