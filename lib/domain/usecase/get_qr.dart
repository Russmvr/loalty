import 'package:loalty/core/either/either.dart';
import 'package:loalty/domain/repository/qr_repository.dart';

class GetQrUsecase{
    const GetQrUsecase({required IQrRepository qrRepository}) : _qrRepository = qrRepository;

    final IQrRepository _qrRepository;

    Either<String> getOrders(){
      return Either.tryCreate(() => _qrRepository.getQr());
    }
}