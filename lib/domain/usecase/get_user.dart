import 'package:loalty/core/either/either.dart';
import 'package:loalty/domain/models/user.dart';
import 'package:loalty/domain/repository/auth_repository.dart';

class GetUserUsecase{
    const GetUserUsecase({required IAuthRepository authRepository}) :
     _authRepository = authRepository;
    final IAuthRepository _authRepository;

    Either<User> getOrders(){
      return Either.tryCreate(() => _authRepository.getUser());
    }
}