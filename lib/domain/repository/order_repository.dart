import 'package:loalty/domain/models/order.dart';

abstract class IOrderRepository{
  List<Order> getOrders();

}