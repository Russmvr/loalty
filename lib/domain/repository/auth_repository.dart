import 'package:loalty/domain/models/user.dart';

abstract class IAuthRepository{
  void login({required String login, required String password});
  void logout({required int id});
  void register({required User user});
  User getUser();
  bool checkAuthorized();

}