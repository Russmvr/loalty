import 'package:hive/hive.dart';

part 'user.g.dart';

@HiveType(typeId: 0)
class User extends HiveObject{
  User({required this.login, required this.password, this.id, required this.name, required this.isAuth});

  @HiveField(0)
  final int? id;

  @HiveField(1)
  final String name;

  @HiveField(2)
  final String login;

  @HiveField(3)
  final String password;

  @HiveField(4)
  final bool isAuth;

  User copyWithChangedAuth(bool isAuth){
    return User(password: this.password, id: this.id, login: this.login, name: this.name, isAuth: isAuth);
  }

  User copyWithChangedId(int id){
    return User(password: this.password, id: id, login: this.login, name: this.name, isAuth: this.isAuth);
  }
}