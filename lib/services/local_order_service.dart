import 'package:loalty/domain/models/order.dart';

class LocalOrderService{
  LocalOrderService();

  final List<Order> _orders = [Order(title: 'Order 1' , 
                              description: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form'),
                              Order(title: 'Order 2' , 
                              description: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form'),
                              Order(title: 'Order 3' , 
                              description: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form'),
                              Order(title: 'Order 4' , 
                              description: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form'),
                              Order(title: 'Order 5' , 
                              description: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form'),
                              Order(title: 'Order 6' , 
                              description: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form'),
                              Order(title: 'Order 7' , 
                              description: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form'),];

    List<Order> getOrders(){
      return _orders;
    }

}