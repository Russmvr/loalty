import 'dart:math';

import 'package:loalty/core/exception/auth_exception.dart';
import 'package:loalty/domain/models/user.dart';
import '../storage/local_auth_storage.dart';

class LocalAuthService {
  LocalAuthService({required LocalAuthStorage localAuthStorage})
      : _localAuthStorage = localAuthStorage;

  final LocalAuthStorage _localAuthStorage;

  bool checkAuthorized() {
    bool _isAuth = false;
    List<User> allUsers = _localAuthStorage.getAllUsers();
    allUsers.forEach((e) {
      if (e.isAuth) _isAuth = true;
    });
    return _isAuth;
  }

  User getUser() {
    final allUsers = _localAuthStorage.getAllUsers();
    final User _user = allUsers.firstWhere((e) => e.isAuth);
    return _user;
  }

  void login({required String login, required String password}) {
    List<User> allUsers = _localAuthStorage.getAllUsers();
    if (allUsers.isNotEmpty) {
      final user = checkAndGetUserByLogin(allUsers, login);
      checkUserByPassword(user.password, password);
      allUsers = setAuthorizedStatus(allUsers, login);
      _localAuthStorage.updateData(allUsers);
    } else {
      throw NoUserFoundException();
    }
  }


  void logout() {
    List<User> allUsers = _localAuthStorage.getAllUsers();
   allUsers = allUsers.map((e) {
      if (e.isAuth) e.copyWithChangedAuth(false);
      return e;
    }).toList();
  }

  void register(User user) {
    try{
    List<User> allUsers = _localAuthStorage.getAllUsers();
    print(allUsers);
    List<int> usersId = allUsers.map((e) => e.id!).toList();
    int generatedId = generateId(usersId);
    user = user.copyWithChangedId(generatedId);
    _localAuthStorage.addUser(user); }
    catch (e){
      print('e2 - $e');
    }
  }

  List<User> setAuthorizedStatus(List<User> allUsers, String login){
    allUsers = allUsers.map((e) {
        if(e.login == login) e.copyWithChangedAuth(true);
        return e;
      }).toList();
      return allUsers;
  }

  User checkAndGetUserByLogin(List<User> allUsers, String login) {
    try {
      User user = allUsers.firstWhere((e) => e.login == login);
      return user;
    } catch (e) {
      throw NoUserFoundException();
    }
  }

  void checkUserByPassword(String passwordFromDB, String passwordFromClient) {
    if (passwordFromDB != passwordFromClient) throw InvalidPasswordException();
  }

  int generateId(List<int> usersId){
    int randomNumber = Random().nextInt(1000);            // GAVNO CODE
    if(usersId.isEmpty) return randomNumber;
    for(int i=0;i<1;){
      randomNumber =Random().nextInt(1000); 
      if(usersId.contains(randomNumber)) continue;
      else i++;
    }
    return randomNumber;
    
  }
}
