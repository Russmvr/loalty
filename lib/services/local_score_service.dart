import 'dart:math';

class LocalScoreService{
  LocalScoreService();

  int _createRandomScore(){
    final randomNumber = Random();
    return randomNumber.nextInt(100);
  }

  int getScore(){
    return _createRandomScore();
  }
}