import 'package:loalty/domain/models/order.dart';
import 'package:loalty/domain/repository/order_repository.dart';
import 'package:loalty/services/local_order_service.dart';

class OrderRepository implements IOrderRepository{
  const OrderRepository({required LocalOrderService localOrderService}) : _localOrderService = localOrderService;


  final LocalOrderService _localOrderService;
  @override
  List<Order> getOrders() {
    return _localOrderService.getOrders();
  }

}