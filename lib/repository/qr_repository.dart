import 'package:loalty/domain/repository/qr_repository.dart';
import 'package:loalty/services/local_qr_service.dart';

class QrRepository implements IQrRepository{
  const QrRepository({required LocalQrService localQrService}) : _localQrService = localQrService;

  final LocalQrService _localQrService;

  @override
  String getQr() {
    return _localQrService.getQrString();
  }

}