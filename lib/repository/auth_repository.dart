import 'package:loalty/domain/models/user.dart';
import 'package:loalty/domain/repository/auth_repository.dart';
import 'package:loalty/services/local_auth_service.dart';

class AuthRepository implements IAuthRepository{

  AuthRepository({required LocalAuthService localAuthService}) : _localAuthService = localAuthService;

  final LocalAuthService _localAuthService;

  @override
  bool checkAuthorized() {
    return _localAuthService.checkAuthorized();
  }

  @override
  User getUser() {
    return _localAuthService.getUser();
  }

  @override
  void login({required String login, required String password}) {
      _localAuthService.login(login: login, password: password);
    }
  
    @override
    void logout({required int id}) {
      // TODO: implement logout
    }
  
    @override
    void register({required User user}) {
    _localAuthService.register(user);
  }
  

}