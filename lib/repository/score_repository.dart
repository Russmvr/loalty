import 'package:loalty/domain/repository/score_repository.dart';
import 'package:loalty/services/local_score_service.dart';

class ScoreRepository implements IScoreRepository{
  const ScoreRepository({required LocalScoreService localScoreService}) : _localScoreService = localScoreService;

  final LocalScoreService _localScoreService;
  

  @override
  int getScore() {
    return _localScoreService.getScore();
  }

}