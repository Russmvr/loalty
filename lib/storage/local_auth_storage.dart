import 'package:hive_flutter/hive_flutter.dart';
import 'package:loalty/domain/models/user.dart';

class LocalAuthStorage {
  LocalAuthStorage({required Box<List<User>> box}) : _box = box;

  static String _boxName = 'users';
  static String _boxKey = 'lists';

  final Box<List<User>> _box;

  List<User> getAllUsers() {
    // _box.put(_boxKey, []);

    List<User>? allUsers = _box.get(_boxKey);
    if (allUsers == null) {
      _box.put(_boxKey, []);
      return [];
    }
    return allUsers;
  }

  void addUser(User user) {
    List<User> allUsers = getAllUsers();
    allUsers.add(user);
    _box.put(_boxKey, allUsers);
  }

  void updateData(List<User> allUsers) => _box.put(_boxKey, allUsers);
}
